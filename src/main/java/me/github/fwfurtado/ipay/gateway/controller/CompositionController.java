package me.github.fwfurtado.ipay.gateway.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalTime;
import java.util.Map;

@RestController
public class CompositionController {

    @GetMapping("composition")
    Map<String, Object> composition() {
        // request pagamentos
        // request pedidos
        // junta o resultado
        return Map.of("time", LocalTime.now());
    }
}
